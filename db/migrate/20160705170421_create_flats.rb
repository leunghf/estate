class CreateFlats < ActiveRecord::Migration
  def change
    create_table :flats do |t|
      t.string :block
      t.string :floor
      t.string :flat

      t.timestamps null: false
    end
  end
end
