require 'csv'

class Flat < ActiveRecord::Base
  def self.import(file)
    CSV.foreach(file.path, :encoding => 'windows-1251:utf-8', headers: true) do |row|
      flat = find_by_id(row["id"]) || new
      flat.attributes = row.to_hash
      flat.save!
    end
  end
end
