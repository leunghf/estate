json.array!(@flats) do |flat|
  json.extract! flat, :id, :block, :floor, :flat
  json.url flat_url(flat, format: :json)
end
